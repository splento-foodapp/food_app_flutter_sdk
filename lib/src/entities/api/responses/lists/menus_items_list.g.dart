// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menus_items_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MenusItemsList _$MenusItemsListFromJson(Map<String, dynamic> json) {
  return MenusItemsList(
    numOfResults: json['num_of_results'] as int,
    numOfPages: json['num_of_pages'] as int,
    limit: json['limit'] as int,
    page: json['page'] as int,
    results: (json['results'] as List)
        ?.map((e) =>
            e == null ? null : MenuItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$MenusItemsListToJson(MenusItemsList instance) =>
    <String, dynamic>{
      'num_of_results': instance.numOfResults,
      'num_of_pages': instance.numOfPages,
      'limit': instance.limit,
      'page': instance.page,
      'results': instance.results?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
