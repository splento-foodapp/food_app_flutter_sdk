// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reference_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReferenceItem _$ReferenceItemFromJson(Map<String, dynamic> json) {
  return ReferenceItem(
    id: json['id'] as String,
    name: json['name'] as String,
    modeId: json['mode_id'] as String,
    companyId: json['company_id'] as String,
    numOfDishes: json['num_of_dishes'] as int,
    dishesShape: json['dishes_shape'] as String,
    referenceImageUrl: json['reference_image_url'] as String,
    referenceImageMediumUrl: json['reference_image_medium_url'] as String,
    referenceImageSmallUrl: json['reference_image_small_url'] as String,
    referenceOverlayUrl: json['reference_overlay_url'] as String,
    referenceOverlayMediumUrl: json['reference_overlay_medium_url'] as String,
    referenceOverlaySmallUrl: json['reference_overlay_small_url'] as String,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$ReferenceItemToJson(ReferenceItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'mode_id': instance.modeId,
      'company_id': instance.companyId,
      'num_of_dishes': instance.numOfDishes,
      'dishes_shape': instance.dishesShape,
      'reference_image_url': instance.referenceImageUrl,
      'reference_image_medium_url': instance.referenceImageMediumUrl,
      'reference_image_small_url': instance.referenceImageSmallUrl,
      'reference_overlay_url': instance.referenceOverlayUrl,
      'reference_overlay_medium_url': instance.referenceOverlayMediumUrl,
      'reference_overlay_small_url': instance.referenceOverlaySmallUrl,
      'error': instance.error,
    };
