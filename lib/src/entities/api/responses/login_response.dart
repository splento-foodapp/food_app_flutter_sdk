import 'package:json_annotation/json_annotation.dart';
part 'login_response.g.dart';
/// Login Response
///
///
@JsonSerializable(explicitToJson: true)
class LoginResponse {
   /// UUID for logged user
   @JsonKey(name: 'id', nullable: false)
   String userId;
   /// Token for logged user
   @JsonKey(name: 'token', nullable: false)
   String token;

   /// Error message for operation
   @JsonKey(name: 'error', nullable: true)
   String error;

   LoginResponse({this.userId, this.token, this.error});

   factory LoginResponse.fromJson(Map<String, dynamic> json) => _$LoginResponseFromJson(json);

   Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}
