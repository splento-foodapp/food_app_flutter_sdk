// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_group_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserGroupItem _$UserGroupItemFromJson(Map<String, dynamic> json) {
  return UserGroupItem(
    id: json['id'] as String,
    name: json['name'] as String,
  )..error = json['error'] as String;
}

Map<String, dynamic> _$UserGroupItemToJson(UserGroupItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'error': instance.error,
    };
