import 'package:foodapp_api_splento/src/entities/api/responses/menu_item.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:json_annotation/json_annotation.dart';

part 'menus_items_list.g.dart';

/// Menus List
///
@JsonSerializable(explicitToJson: true)
class MenusItemsList {
  @JsonKey(name: 'num_of_results')
  int numOfResults;

  @JsonKey(name: 'num_of_pages')
  int numOfPages;

  int limit;

  int page;

  List<MenuItem> results;

  String error;

  MenusItemsList({this.numOfResults, this.numOfPages, this.limit, this.page, List<MenuItem> results, this.error})
    : results = results ?? <MenuItem>[];

  factory MenusItemsList.fromJson(Map<String, dynamic> json) => _$MenusItemsListFromJson(json);

  Map<String, dynamic> toJson() => _$MenusItemsListToJson(this);

}