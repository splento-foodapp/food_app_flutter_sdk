import 'dart:developer';

part 'conts_env.dart';

final String _fullApiServerUrl = '';
final String _fullApiServerSandboxUrl = 'https://foodapi.splento.com';

final String _apiHost = 'foodapi.splento.com';
final String _apiHostSandBox = 'foodapi.splento.com';


final Map<String, String> photoStatuses = {
    'create': 'created',
    'uploaded': 'uploaded',
    'retouched': 'retouched',
    'manual_check_fail': 'manual_check_fail'
};

final String _apiPrefix = '/api/v1/';

/// For sandbox mode [isDebug] should be set as true, or as false for live mode
bool isDebug = true;

/// Full list api points with alias
final Map<String, dynamic> apiPoints = {
  /// Init request
  'init'          : 'init',
  /// Auth data routes
  'auth.login'          : 'user/login',
  'auth.logout'   : 'user/logout',
  /// User data routes
  'user.info'     : 'user/_uuid_',

  /// Photo
  'photo.list'     : 'photo/list/_uuid_',
  'photo.check'    : 'photo/check',
  'photo.get'      : 'photo/_uuid_',
  'photo.create'   : 'photo/create',
  'photo.update'   : 'photo/_uuid_/update',
  'photo.delete'   : 'photo/_uuid_/update',

  /// Notifications
  'notification.list'     : 'notification/list/_uuid_',
  'notification.get'      : 'notification/_uuid_',
  /// Examples
  'example.list'     : 'example/list/_uuid_',
  'example.get'      : 'example/_uuid_',
  /// References
  'reference.list'     : 'reference/list/_uuid_',
  'reference.get'      : 'reference/_uuid_',
  /// Menus
  'menu.list'     : 'menu/list/_uuid_',
  'menu.get'      : 'menu/_uuid_',
  /// Menu Items
  'menuItem.list'     : 'menuitem/list/_uuid_',
  'menuItem.get'      : 'menuitem/_uuid_',
  'menuItem.create'   : 'menuitem/create',
  'menuItem.update'   : 'menuitem/_uuid_/update',
  'menuItem.delete'   : 'menuitem/_uuid_/delete',





};


/// Call this method for create full url to api point look at [apiPoints]
String getServerUrl(String action, [String uuid]) {
  if ( !apiPoints.containsKey(action) ) {
    throw Exception("Can't find api point");
  }

  final fullPathApi = isDebug ? _fullApiServerSandboxUrl : _fullApiServerUrl;

  if ( uuid != null ) {
    return "${fullPathApi}${_apiPrefix}${apiPoints[action]
        .toString()
        .replaceAll("_uuid_", uuid)}";
  }
  return "${fullPathApi}${_apiPrefix}${apiPoints[action]}";

}

Uri makeUriForRequest(String action) {

  if ( !apiPoints.containsKey(action) ) {
    throw Exception("Can't find api point");
  }
  var uri = new Uri(scheme: 'https', host: isDebug ? _apiHostSandBox : _apiHost, path: "${_apiPrefix}${apiPoints[action]}");
  return uri;
}