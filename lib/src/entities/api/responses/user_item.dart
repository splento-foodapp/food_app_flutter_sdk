import 'user_group_item.dart';
import 'company_item.dart';
import 'menu_item.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';

part 'user_item.g.dart';

/// User Item Object
///
///
@JsonSerializable(explicitToJson: true)
class UserItem {

  String id;

  @JsonKey(name: 'first_name')
  String firstName;
  @JsonKey(name: 'last_name')
  String lastName;

  String email;
  String phone;

  /// Lat, Lng coordinates for user
  @JsonKey(name: 'last_coord')
  String locationCoordinates;
  /// Name of Location
  @JsonKey(name: 'location_name')
  String locationName;

  /// Relations
  ///
  ///
  /// List of [UserGroupItem]
  List<UserGroupItem> groups;
  /// Lis of [CompanyItem]
  List<CompanyItem> companies;
  /// List of [Menu]
  List<Menu> menus;


  // Extended
  String error;

  UserItem({ this.id, this.firstName, this.lastName, this.phone, this.email,
    this.locationCoordinates, this.locationName,
    List<UserGroupItem> groups,
    List<CompanyItem> companies, List<Menu> menus,
    this.error})
  : groups = groups ?? <UserGroupItem>[],
  companies = companies ?? <CompanyItem>[],
  menus = menus ?? <Menu>[];

  factory UserItem.fromJson(Map<String, dynamic> json) => _$UserItemFromJson(json);



}