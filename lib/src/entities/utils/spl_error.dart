
/// Basic Error object
class SPLError {
  String uuid;
  String message;
  int code;
  bool isShow = false;


  SPLError(String uuid, String message, [int code = 0]) {
    this.uuid = uuid;
    this.message = message;
    this.code = code;

  }


  @override
  String toString() {
    this.isShow = true;
    return "[SPL_API_ERROR]: ${(this.code > 0 ? "(${this.code})": "")}${this.message}. [UUID]: ${this.uuid}";
  }


}