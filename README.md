# foodapp_api_splento

SPLENTO FOOOD API SDK

Version `0.0.1` (and higher) requires Flutter `>= 1.20.0` and Dart `>= 2.9.1`.

## Features

- Initialization data
- Login / Logout User
- User Info
- Company data ( in UserInfo )
- Menus
- Menu Items (CRUD)
- Notifications
- References
- Examples
- Photos (CRUD)


## Getting started

You should ensure that you add the router as a dependency in your flutter project.
```yaml
dependencies:
 foodapp_api_splento:
   git: git@gitlab.com:splento-foodapp/food_app_flutter_sdk.git
```
You should then run `flutter packages upgrade` or update your packages in IntelliJ.

## Setting up

First, you should define a new `SPLSdk` object by initializing it as such:

```dart
final splentoSDK = SPLSdk();
splentoSDK.debugMode(true); /// For sandbox mode

```
Also you should create  `lib/src/entities/utils/conts_env.dart` with content:
```dart
part of 'conts.dart';

final String testUser = "your_login_sandbox";
final String testPassword = "your_password_sandbox";
```
These variables can be left blank


It may be convenient for you to store the router globally/statically so that
you can access the SDK in other areas in your application.

## Examples

- Initialization

```dart
SPLSdk sdk = SPLSdk();
await sdk.initializeData();
await sdk.login( testUser,  testPassword);
```
`sdk.login()` - method gets user data on successful authorization

- Check photo
```dart
var fileImage = new File('./assets/images/test_image.jpg');
PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
final lResponse = await sdk.rest.photoCheckAction(phRequest);
```

- Create photo

```dart

String menuId;
ModeItem mode;
PhotoItem photoForTest = PhotoItem();

menuId = sdk.getUserData().menus.first.id;
mode = sdk.getUserData().companies.first.modes.first;

/// Fill photo data
photoForTest.menuId = menuId;
photoForTest.modeId = mode.id;
photoForTest.fileName = 'from_unit_test.jpg';
photoForTest.rawFileUrl = 'https://domain.name/New_Tab_2020-09-05_20-39-27.jpg';
photoForTest.aspectRatioY = aspectRatioY;
photoForTest.aspectRatioX = aspectRatioX;

photoForTest.angleV = angleV;
photoForTest.angleH = angleH;

final lResponse = await sdk.rest.photoCreateAction(photoForTest);
photoForTest = lResponse;
```


You can find more examples in `test` folder


## Extend

if you want you can generate package API documentation  run in console this command
`flutter pub global run dartdoc:dartdoc`,  and
`flutter pub global run dhttpd --path doc/api` - for create http server with docs
( default path http://localhost:8080 )