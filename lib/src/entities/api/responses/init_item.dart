
import 'package:json_annotation/json_annotation.dart';
part 'init_item.g.dart';

@JsonSerializable(explicitToJson: true)
class InitItem {
  /// Sort by catalog
  @JsonKey(name: 'sort_by')
  Map<String, dynamic> sortBy;
  /// Limit catalog
  Map<String, dynamic> limit;
  /// Photo Statuses
  @JsonKey(name: 'photo_statuses')
  Map<String, dynamic> photoStatuses;
  /// Automated check Statuses
  @JsonKey(name: 'automated_check_statuses')
  Map<String, dynamic> automatedCheckStatuses;

  /// Notification Types
  @JsonKey(name: 'notification_types')
  Map<String, dynamic> notificationTypes;

  /// Dish Shape Types
  @JsonKey(name: 'dish_shape_types')
  Map<String, dynamic> dishShapeTypes;

  String error;


  InitItem({ Map<String, dynamic> sortBy, Map<String, dynamic> limit,
    Map<String, dynamic> photoStatuses,
    Map<String, dynamic> automatedCheckStatuses,
    Map<String, dynamic> notificationTypes,
    Map<String, dynamic> dishShapeTypes,
    this.error})
    : sortBy  = sortBy ?? Map<String, dynamic>(),
      limit  = limit ?? Map<String, dynamic>(),
      photoStatuses  = photoStatuses ?? Map<String, dynamic>(),
      automatedCheckStatuses  = automatedCheckStatuses ?? Map<String, dynamic>(),
        dishShapeTypes  = dishShapeTypes ?? Map<String, dynamic>(),
      notificationTypes  = notificationTypes ?? Map<String, dynamic>();


  factory InitItem.fromJson(Map<String, dynamic> json) => _$InitItemFromJson(json);

  Map<String, dynamic> toJson() => _$InitItemToJson(this);

}