// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoItem _$PhotoItemFromJson(Map<String, dynamic> json) {
  return PhotoItem(
    id: json['id'] as String,
    photoStatus: json['photo_status'] as String,
    splentoRawFileId: json['splento_raw_file_id'] as String,
    manualCheckFailReason: json['manual_check_fail_reason'] as String,
    automatedCheckFailReason: json['automated_check_fail_reason'] as String,
    automatedCheckStatus: json['automated_check_status'] as String,
    fileName: json['file_name'] as String,
    rawFileUrl: json['raw_file_url'] as String,
    rawFilePreviewUrl: json['raw_file_preview_url'] as String,
    rawFileThumbUrl: json['raw_file_thumb_url'] as String,
    retouchedFileUrl: json['retouched_file_url'] as String,
    retouchedFilePreviewUrl: json['retouched_file_preview_url'] as String,
    retouchedFileThumbUrl: json['retouched_file_thumb_url'] as String,
    aspectRatioX: json['aspect_ratio_x'] as int,
    aspectRatioY: json['aspect_ratio_y'] as int,
    angleV: json['angle_v'] as int,
    angleH: json['angle_h'] as int,
    menuId: json['menu_id'] as String,
    menuItemId: json['menu_item_id'] as String,
    modeId: json['mode_id'] as String,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$PhotoItemToJson(PhotoItem instance) => <String, dynamic>{
      'id': instance.id,
      'photo_status': instance.photoStatus,
      'splento_raw_file_id': instance.splentoRawFileId,
      'manual_check_fail_reason': instance.manualCheckFailReason,
      'automated_check_status': instance.automatedCheckStatus,
      'automated_check_fail_reason': instance.automatedCheckFailReason,
      'file_name': instance.fileName,
      'raw_file_url': instance.rawFileUrl,
      'raw_file_preview_url': instance.rawFilePreviewUrl,
      'raw_file_thumb_url': instance.rawFileThumbUrl,
      'retouched_file_url': instance.retouchedFileUrl,
      'retouched_file_preview_url': instance.retouchedFilePreviewUrl,
      'retouched_file_thumb_url': instance.retouchedFileThumbUrl,
      'aspect_ratio_x': instance.aspectRatioX,
      'aspect_ratio_y': instance.aspectRatioY,
      'angle_v': instance.angleV,
      'angle_h': instance.angleH,
      'menu_id': instance.menuId,
      'menu_item_id': instance.menuItemId,
      'mode_id': instance.modeId,
      'error': instance.error,
    };
