import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/entities.dart';
import 'package:foodapp_api_splento/src/handlers/handlers.dart';
import 'package:foodapp_api_splento/src/services/services.dart';

import 'package:uuid/uuid.dart';

import 'dart:developer' as developer;

void main() {
  group('[SPLENTO]: Rest service', ()
  {
    SPLSdk sdk;
    String menuId;
    ModeItem mode;
    MenuItem menuForTest = MenuItem();
    
    developer.log('Test Test');

    test('Rest Service - Init', () {
      sdk = SPLSdk();
      expect(sdk.rest.checkIfObjectIsNotNull(), true);
    });

    test('Check init is initialized', () async {
      await sdk.initializeData();
      expect(sdk.initData, isNot(null));
    });


    test('Rest Service - Init Action', () async {
      final initResponse =  await sdk.rest.initAction();
      sdk.initData = initResponse;
      expect(initResponse, isNot(null));
    });


    test('Rest Service - Success Login', () async {
      LoginRequest form = LoginRequest(email: testUser, password: testPassword, deviceId: '', devicePlatform: 'none');
      final lResponse = await sdk.rest.loginAction(form);
      expect(lResponse.token, isNot(null));
    });

    test('Rest Service - Success getting user Info', () async {
      final lResponse = await sdk.rest.userInfoAction(sdk.rest.getUserId());
      sdk.setUserData(lResponse);
      expect(lResponse.id, isNot(null));
    });


    /// List menu items
    test('Rest Service - List menu items', () async {
      String menuId = sdk.getUserData().menus.first.id;
      final lResponse = await sdk.rest.menuItemListAction(menuId);
      expect(lResponse.error, null);
    });


    /// Create menu item
    ///
    test('Rest Service - Create menu item', () async {
      menuId = sdk.getUserData().menus.first.id;

      /// Fill photo data
      menuForTest.menuId = menuId;
      menuForTest.name = "From Unit Test (will be deleted)";
      menuForTest.description = "Description Test";

      final lResponse = await sdk.rest.menuItemCreateAction(menuForTest);
      menuForTest = lResponse;
      expect(lResponse.id, isNot(null));
    });

    /// Get menu item
    test('Rest Service - Get  menu item', () async {
      final lResponse = await sdk.rest.menuItemInfoAction(menuForTest.id);
      menuForTest = lResponse;
      expect(lResponse.error, null);
    });

    /// Get menu item (fail)
    test('Rest Service - Get menu item fail', () async {
      final lResponse = await sdk.rest.menuItemInfoAction('xxx');
      expect(lResponse.error, isNot(null));
    });

    /// Update menu item
    test('Rest Service - Update menu item', () async {
      String newName = 'Marked for delete';
      menuForTest.name = newName;
      final lResponse = await sdk.rest.menuItemUpdateAction(menuForTest);
      menuForTest = lResponse;
      expect(lResponse.name, newName);
    });

    /// Delete menu item
    test('Rest Service - Delete menu item', () async {
      final lResponse = await sdk.rest.menuItemDeleteAction(menuForTest);
      menuForTest = lResponse;
      expect(lResponse.error, null);
    });


  });
}
