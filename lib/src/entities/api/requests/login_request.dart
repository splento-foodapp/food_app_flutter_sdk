/// Login Response class
///
class LoginRequest {

  final String email;
  final String password;

  String deviceId;
  String devicePlatform;

  LoginRequest({this.email, this.password, this.deviceId, this.devicePlatform});

  LoginRequest.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        deviceId = json['device_id'],
        devicePlatform = json['device_platform'];

  Map<String, dynamic> toJson() =>
      {
        'email': email,
        'password': password,
        'device_id': deviceId,
        'device_platform': devicePlatform
      };

}