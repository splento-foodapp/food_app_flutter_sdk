import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/entities.dart';
import 'package:foodapp_api_splento/src/handlers/handlers.dart';
import 'package:foodapp_api_splento/src/services/services.dart';

import 'package:uuid/uuid.dart';

import 'dart:developer' as developer;

void main() {
  group('[SPLENTO]: Rest service', ()
  {
    SPLSdk sdk;
    String menuId;
    ModeItem mode;
    Menu menuForTest = Menu();
    
    developer.log('Test Test');

    test('Rest Service - Init', () {
      sdk = SPLSdk();
      expect(sdk.rest.checkIfObjectIsNotNull(), true);
    });

    test('Check init is initialized', () async {
      await sdk.initializeData();
      expect(sdk.initData, isNot(null));
    });


    test('Rest Service - Init Action', () async {
      final initResponse =  await sdk.rest.initAction();
      sdk.initData = initResponse;
      expect(initResponse, isNot(null));
    });


    test('Rest Service - Success Login', () async {
      LoginRequest form = LoginRequest(email: testUser, password: testPassword, deviceId: '', devicePlatform: 'none');
      final lResponse = await sdk.rest.loginAction(form);
      expect(lResponse.token, isNot(null));
    });

    test('Rest Service - Success getting user Info', () async {
      final lResponse = await sdk.rest.userInfoAction(sdk.rest.getUserId());
      sdk.setUserData(lResponse);
      expect(lResponse.id, isNot(null));
    });

  
    /// List
    test('Rest Service - List menus', () async {
      String companyId = sdk.getUserData().companies.first.id;
      final lResponse = await sdk.rest.menuListAction(companyId);
      if ( lResponse.results.isNotEmpty ) {
        menuForTest = lResponse.results.first;
      }
      expect(lResponse.error, null);
    });


    /// Get menu
    test('Rest Service - Get menu', () async {
      if (menuForTest.id != null) {
        final lResponse = await sdk.rest.menuInfoAction(
            menuForTest.id);
        menuForTest = lResponse;
        expect(lResponse.error, null);
      } else {
        // Menus list is empty
        expect(true, true);
      }

    });







  });
}
