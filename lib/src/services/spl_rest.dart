import 'dart:convert';
import 'dart:core';
import 'dart:developer';
import 'package:foodapp_api_splento/src/entities/api/responses/lists/examples_list.dart';
import 'package:foodapp_api_splento/src/entities/api/responses/lists/menus_items_list.dart';
import 'package:foodapp_api_splento/src/entities/api/responses/lists/menus_list.dart';
import 'package:foodapp_api_splento/src/entities/api/responses/lists/references_list.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:uuid/uuid.dart';

import 'package:foodapp_api_splento/src/entities/entities.dart';
import 'package:foodapp_api_splento/src/entities/api/api.dart';
import 'package:foodapp_api_splento/src/entities/utils/auth_headers.dart';
import 'package:foodapp_api_splento/src/handlers/handlers.dart';
import 'package:foodapp_api_splento/src/entities/utils/conts.dart' as consts;


/// Splento Rest service for Flutter Splento SDK
///
///
class SPLRest {
  static final SPLRest _instance = SPLRest._internal();

  /// instance for SPLErrors service
  SPLErrors errors = SPLErrors();

  String _token = '';
  String _userId = '';

  final String nameHeaderApiToken = 'X-Api-Token';

  Map<String, String> lHeaders = Map<String, String>();

  AuthHeaders authHeaders = AuthHeaders();

  /// UUID for error in last request (if exist)
  String lastErrorUuid;

  String getToken() {
    return this._token;
  }

  void _setToken(String token) {
    this._token = token;
  }


  void setUserId(String id) {
    this._userId = id;
  }

  String getUserId() {
    return this._userId;
  }



  factory SPLRest() {
    /// Init headers
    _instance.lHeaders.putIfAbsent('Content-Type', () => 'application/json');
    _instance.lHeaders.putIfAbsent('Accept', () => 'application/json');

    _instance.authHeaders.headers = _instance.lHeaders;

    return _instance;
  }

  SPLRest._internal();
  
  void toolsCheckResponseCode(http.Response response) {
    /// If server can't response
    if ( response.statusCode == 500 ) {
      throw Exception('Problem with API server. Try again later');
    }
    /// If current token is unvalid
    if ( response.statusCode == 403 ) {
      // Clear token data
      this._token = null;
      this.lHeaders.remove(nameHeaderApiToken);
    } 
  }

  /// @nodoc
  bool checkIfObjectIsNotNull() {
    return this != null ? true : false;
  }

  /// Initialization Action for get services data about limits,
  /// dishes shapes, sorting information  e.t.c
  ///
  Future<InitItem> initAction() async {
    final response = await http.get(consts.getServerUrl('init'),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    InitItem lResponse = InitItem.fromJson(jsonDecode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    } else {
      return lResponse;
    }

  }






  /// Login to API Server with email and password
  /// 
  Future<LoginResponse> loginAction(LoginRequest form)  async {
    final response = await http.post(consts.getServerUrl('auth.login'),
        headers: this.lHeaders,
        body: jsonEncode(form.toJson()));
  
    this.toolsCheckResponseCode(response);

    
    LoginResponse lResponse = LoginResponse.fromJson(json.decode(response.body));

    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    } else {
      if ( lResponse.token != null ) {
        /// Fill current token
        this._setToken(lResponse.token);
        /// Fill headers for next requests
        this.lHeaders.putIfAbsent(_instance.nameHeaderApiToken, () => this.getToken());
        this.authHeaders.headers = this.lHeaders;

      }
      if ( lResponse.userId != null ) {
        this.setUserId(lResponse.userId);
      }
    }
    return lResponse;
    
  }


  /// Get full user info by ID
  ///
  Future<UserItem> userInfoAction(String id) async {
    final response = await http.get("${consts.getServerUrl('user.info', id)}",
        headers: this.authHeaders.headers);

    this.toolsCheckResponseCode(response);

    UserItem lResponse = UserItem.fromJson(jsonDecode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    } else {
      return lResponse;
    }
  }


  /// PHOTOS

  /// Send Image (binary) ( jpg ) for check
  /// as result some information for user about quality of photo
  /// Look Examples in test/02_0_spl_sdk_rest_test.dart - Rest Service - Photo check
  Future<PhotoCheckResponse> photoCheckAction(PhotoCheckRequest form) async {
    Uri photoCheckUri = consts.makeUriForRequest('photo.check');
    String photoCheckString = photoCheckUri.toString();
    var requestMP = http.MultipartRequest("POST", photoCheckUri);
    requestMP.headers.addAll(authHeaders.headers);
    requestMP.files.add(http.MultipartFile.fromBytes('photo', form.photo, filename: 'photo_for_check.jpg', contentType: new MediaType('image', 'jpg')));
    http.Response response = await http.Response.fromStream(await requestMP.send());
    PhotoCheckResponse phResponse = new PhotoCheckResponse.fromJson(jsonDecode(response.body));
    if ( response.statusCode != 200 ) {
      this._makeError("Problem with Photo Status");
      phResponse.status = 'FAIL';
      phResponse.messages.add("Problem with server connection");
    }
    return phResponse;
  }

  /// List Photos by Menu ID
  ///
  /// Required parameters
  /// [menuId] - menu id
  ///
  Future<PhotosList> photoListAction(String menuId) async {
    if ( menuId == null  || menuId.isEmpty ) {
      throw Exception("menuId is required");
    }

    final response = await http.get(consts.getServerUrl('photo.list', menuId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    PhotosList lResponse = PhotosList.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Get Photo Item
  ///
  Future<PhotoItem> photoInfoAction(String photoId) async {
    if ( photoId == null  || photoId.isEmpty ) {
      throw Exception("photoId is required");
    }

    final response = await http.get(consts.getServerUrl('photo.get', photoId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    PhotoItem lResponse = PhotoItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Create Photo Item
  ///
  /// Required parameters
  /// [PhotoItem.menuId] - menu id
  /// [PhotoItem.modeId] - current mode id
  /// [PhotoItem.fileName] - string
  /// [PhotoItem.rawFileUrl] - full path URL
  /// [PhotoItem.aspectRatioX] - shooting Aspect Ration width ( ex. 16 )
  /// [PhotoItem.aspectRatioY] - shooting Aspect Ration height ( ex. 9 )
  /// [PhotoItem.angleV] - Vertical shooting angle ( INT ) deg.
  /// [PhotoItem.angleH] - Horizontal shooting angle ( INT ) deg.
  ///
  Future<PhotoItem> photoCreateAction(PhotoItem photo) async {

    if ( photo.id != null ) {
       throw Exception("Id must be empty or null");
    }

    photo.photoStatus = photoStatuses['create'];

    final response = await http.post(consts.getServerUrl('photo.create'),
        headers: this.lHeaders,
        body: jsonEncode(photo.toJson()));

    this.toolsCheckResponseCode(response);

    PhotoItem lResponse = PhotoItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Update Photo Item
  ///
  /// Optional parameters
  /// [PhotoItem.menuItemId] - menu item id
  ///
  /// Required parameters
  /// [PhotoItem.id] - id photo item
  /// [PhotoItem.menuId] - menu id
  /// [PhotoItem.modeId] - current mode id
  /// [PhotoItem.fileName] - string
  /// [PhotoItem.rawFileUrl] - full path URL
  /// [PhotoItem.aspectRatioX] - shooting Aspect Ration width ( ex. 16 )
  /// [PhotoItem.aspectRatioY] - shooting Aspect Ration height ( ex. 9 )
  /// [PhotoItem.angleV] - Vertical shooting angle ( INT ) deg.
  /// [PhotoItem.angleH] - Horizontal shooting angle ( INT ) deg.
  ///
  Future<PhotoItem> photoUpdateAction(PhotoItem photo) async {
    if ( photo.id == null  || photo.id.isEmpty ) {
      throw Exception("Id is required");
    }
    final response = await http.post(consts.getServerUrl('photo.update', photo.id),
        headers: this.lHeaders,
        body: jsonEncode(photo.toJson()));

    this.toolsCheckResponseCode(response);

    PhotoItem lResponse = PhotoItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Delete Photo Item
  ///
  /// Required parameters
  /// [PhotoItem.id] - id photo item
  ///
  Future<PhotoItem> photoDeleteAction(PhotoItem photo) async {
    if ( photo.id == null  || photo.id.isEmpty ) {
      throw Exception("Id is required");
    }
    final response = await http.post(consts.getServerUrl('photo.delete', photo.id),
        headers: this.lHeaders,
        body: jsonEncode(photo.toJson()));

    this.toolsCheckResponseCode(response);

    PhotoItem lResponse = PhotoItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }
  /// PHOTOS END
  ///

  /// NOTIFICATIONS
  /// List Notifications by user ID
  ///
  /// Required parameters
  /// [userId] - user id
  ///
  Future<NotificationList> notificationListAction(String userId) async {
    if ( userId == null  || userId.isEmpty ) {
      throw Exception("userId is required");
    }

    final response = await http.get(consts.getServerUrl('notification.list', userId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    NotificationList lResponse = NotificationList.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Get Notification Item
  ///
  Future<NotificationItem> notificationInfoAction(String notificationId) async {
    if ( notificationId == null  || notificationId.isEmpty ) {
      throw Exception("notificationId is required");
    }

    final response = await http.get(consts.getServerUrl('notification.get', notificationId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    NotificationItem lResponse = NotificationItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }
  /// NOTIFICATIONS END


  /// EXAMPLES
  /// List Examples by mode ID
  ///
  /// Required parameters
  /// [modeId] - mode id
  ///
  Future<ExamplesList> exampleListAction(String modeId) async {
    if ( modeId == null  || modeId.isEmpty ) {
      throw Exception("modeId is required");
    }

    final response = await http.get(consts.getServerUrl('example.list', modeId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    ExamplesList lResponse = ExamplesList.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Get Example Item
  ///
  Future<ExampleItem> exampleInfoAction(String exampleId) async {
    if ( exampleId == null  || exampleId.isEmpty ) {
      throw Exception("exampleId is required");
    }

    final response = await http.get(consts.getServerUrl('example.get', exampleId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    ExampleItem lResponse = ExampleItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }
  /// EXAMPLES END


  /// REFERENCES
  /// List References by company ID
  ///
  /// Required parameters
  /// [companyId] - company id
  /// [numOfDishes] - number of dishes
  /// [dishesShape] - dishes shape
  ///
  Future<ReferencesList> referenceListAction(String companyId, int numOfDishes, String dishesShape) async {
    if ( companyId == null  || companyId.isEmpty ) {
      throw Exception("companyId is required");
    }

    final response = await http.get("${consts.getServerUrl('reference.list', companyId)}?num_of_dishes=${numOfDishes}&dishes_shape=${dishesShape}",
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    ReferencesList lResponse = ReferencesList.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Get Reference Item
  ///
  Future<ReferenceItem> referenceInfoAction(String referenceId) async {
    if ( referenceId == null  || referenceId.isEmpty ) {
      throw Exception("referenceId is required");
    }

    final response = await http.get(consts.getServerUrl('reference.get', referenceId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    ReferenceItem lResponse = ReferenceItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }
  /// REFERENCES END


  /// MENUS
  /// List Menus by company ID
  ///
  /// Required parameters
  /// [companyId] - company id
  ///
  Future<MenusList> menuListAction(String companyId) async {
    if ( companyId == null  || companyId.isEmpty ) {
      throw Exception("companyId is required");
    }

    final response = await http.get(consts.getServerUrl('menu.list', companyId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    MenusList lResponse = MenusList.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Get Menu
  ///
  Future<Menu> menuInfoAction(String menuId) async {
    if ( menuId == null  || menuId.isEmpty ) {
      throw Exception("menuId is required");
    }

    final response = await http.get(consts.getServerUrl('menu.get', menuId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    Menu lResponse = Menu.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }
  /// MENUS END
  ///

  /// MENU ITEMS

  /// List Menu Items by Menu ID
  ///
  /// Required parameters
  /// [menuId] - menu id
  ///
  Future<MenusItemsList> menuItemListAction(String menuId) async {
    if ( menuId == null  || menuId.isEmpty ) {
      throw Exception("menuId is required");
    }

    final response = await http.get(consts.getServerUrl('menuItem.list', menuId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    MenusItemsList lResponse = MenusItemsList.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Get Menu Item
  ///
  Future<MenuItem> menuItemInfoAction(String menuItemId) async {
    if ( menuItemId == null  || menuItemId.isEmpty ) {
      throw Exception("menuItemId is required");
    }

    final response = await http.get(consts.getServerUrl('menuItem.get', menuItemId),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    MenuItem lResponse = MenuItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Create Menu Item
  ///
  /// Optional parameters

  ///
  /// Required parameters
  /// [MenuItem.menuId] - menu id
  /// [MenuItem.name] - full name fro menu item
  /// [MenuItem.description] - description menu item
  Future<MenuItem> menuItemCreateAction(MenuItem menuItem) async {

    if ( menuItem.id != null ) {
      throw Exception("Id must be empty or null");
    }

    final response = await http.post(consts.getServerUrl('menuItem.create'),
        headers: this.lHeaders,
        body: jsonEncode(menuItem.toJson()));

    this.toolsCheckResponseCode(response);

    MenuItem lResponse = MenuItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Update Menu Item
  ///
  /// Optional parameters
  ///
  /// Required parameters
  /// [MenuItem.id] - id menu item
  /// [MenuItem.menuId] - menu id
  /// [MenuItem.name] - full name fro menu item
  /// [MenuItem.description] - description menu item
  ///
  Future<MenuItem> menuItemUpdateAction(MenuItem menuItem) async {
    if ( menuItem.id == null  || menuItem.id.isEmpty ) {
      throw Exception("Id is required");
    }
    final response = await http.post(consts.getServerUrl('menuItem.update', menuItem.id),
        headers: this.lHeaders,
        body: jsonEncode(menuItem.toJson()));

    this.toolsCheckResponseCode(response);

    MenuItem lResponse = MenuItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }

  /// Delete Menu Item
  ///
  /// Required parameters
  /// [MenuItem.id] - id menu item
  ///
  Future<MenuItem> menuItemDeleteAction(MenuItem menuItem) async {
    if ( menuItem.id == null  || menuItem.id.isEmpty ) {
      throw Exception("Id is required");
    }
    final response = await http.post(consts.getServerUrl('menuItem.delete', menuItem.id),
        headers: this.lHeaders);

    this.toolsCheckResponseCode(response);

    MenuItem lResponse = MenuItem.fromJson(json.decode(response.body));
    if ( lResponse.error != null && lResponse.error != "" ) {
      // Store info about error to errors service
      this._makeError(lResponse.error);
      return lResponse;
    }

    return lResponse;
  }
  /// MENUS ITEMS END
  ///
  void _makeError(String errorMessage) {
    String lUuid = Uuid().v4();
    this.lastErrorUuid = lUuid;
    SPLError error = SPLError(lUuid, errorMessage);
    this.errors.storeError(lUuid, error);

  }



}