// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompanyItem _$CompanyItemFromJson(Map<String, dynamic> json) {
  return CompanyItem(
    id: json['id'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    ownerId: json['owner_id'] as String,
    inviteCode: json['invite_code'] as String,
    modes: (json['modes'] as List)
        ?.map((e) =>
            e == null ? null : ModeItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    references: (json['references'] as List)
        ?.map((e) => e == null
            ? null
            : ReferenceItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    examples: (json['examples'] as List)
        ?.map((e) =>
            e == null ? null : ExampleItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$CompanyItemToJson(CompanyItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'owner_id': instance.ownerId,
      'invite_code': instance.inviteCode,
      'modes': instance.modes?.map((e) => e?.toJson())?.toList(),
      'references': instance.references?.map((e) => e?.toJson())?.toList(),
      'examples': instance.examples?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
