/// This is a global object that stores messages about all errors received
/// while working with the API. The class presents methods
///  * adding an error **storeError**
///  * error removal **removeError**
///  * getting an error by unique identifier (UUID) **getError**
///  * getting the last received error **getLastError**
///
/// The object must be created before accessing the main API
/// functionality (in addition, the object is created in the REST service)

import 'dart:core';
import 'package:foodapp_api_splento/src/entities/utils/spl_error.dart';


/// SPLENTO Food API Error service
class SPLErrors {

    static final SPLErrors _instance = SPLErrors._internal();

    factory SPLErrors() {
      return _instance;
    }

    SPLErrors._internal();

    /// Global errors collection
    Map<String, SPLError> listErrors = Map<String, SPLError>();
    /// Last added error object
    SPLError lastError;

    /// Store error message with [uuid] and [error] object ([SPLError])
    storeError(String uuid, SPLError error) {
      this.listErrors.putIfAbsent(uuid, () => error);
      this.lastError = error;
    }


    /// Delete error by [uuid]
    removeError(String uuid) {
      if (this.listErrors.isNotEmpty && this.listErrors.containsKey(uuid)) {
          this.listErrors.remove(uuid);
          if ( this.lastError.uuid == uuid ) {
            this.lastError = null;
            this.getLastError();
          }

      } else {
        throw new Exception("[SPLENTO]: Cant't find error ${uuid}");
      }
    }

    /// Search and return last Error ([SPLError]) item
    SPLError getLastError() {

      if (this.listErrors.isEmpty) {
        return null;
      }

      var aL = this.listErrors.length;
      var cI = 0;
      this.listErrors.forEach((key, value) {
        if ( aL == cI ) {
            this.lastError = value;
        }
        cI++;
      });

      return this.lastError;
    }

    /// Get and return error item ([SPLError]) by [uuid],
    /// if item can't be found method returned [Exception]
    SPLError getError(String uuid) {
      if (this.listErrors.isNotEmpty && this.listErrors.containsKey(uuid)) {
        this.listErrors[uuid].isShow = true;
        return this.listErrors[uuid];
      } else {
        throw new Exception("[SPLENTO]: Cant't find error ${uuid}");
      }
    }


    /// Customization toString method
    @override
    String toString() {
      return "[SPLENTO]: Errors Service - contains ${this.listErrors.length} "
          "items \r\n [SPLENTO]: Last item \r\n ${this.lastError != null ? this.lastError.toString() : "NA"}";
    }

    /// @nodoc
    bool checkIfObjectIsNotNull() {
      return this != null ? true : false;
    }


}