// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'example_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExampleItem _$ExampleItemFromJson(Map<String, dynamic> json) {
  return ExampleItem(
    id: json['id'] as String,
    companyId: json['company_id'] as String,
    modeId: json['mode_id'] as String,
    name: json['name'] as String,
    exampleThumbUrl: json['example_small_url'] as String,
    exampleUrl: json['example_url'] as String,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$ExampleItemToJson(ExampleItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'company_id': instance.companyId,
      'mode_id': instance.modeId,
      'name': instance.name,
      'example_small_url': instance.exampleThumbUrl,
      'example_url': instance.exampleUrl,
      'error': instance.error,
    };
