import 'package:foodapp_api_splento/src/entities/api/responses/example_item.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:json_annotation/json_annotation.dart';

part 'examples_list.g.dart';

/// Photos List
///
@JsonSerializable(explicitToJson: true)
class ExamplesList {
  @JsonKey(name: 'num_of_results')
  int numOfResults;

  @JsonKey(name: 'num_of_pages')
  int numOfPages;

  int limit;

  int page;

  List<ExampleItem> results;

  String error;

  ExamplesList({this.numOfResults, this.numOfPages, this.limit, this.page, List<ExampleItem> results, this.error})
    : results = results ?? <ExampleItem>[];

  factory ExamplesList.fromJson(Map<String, dynamic> json) => _$ExamplesListFromJson(json);

  Map<String, dynamic> toJson() => _$ExamplesListToJson(this);

}