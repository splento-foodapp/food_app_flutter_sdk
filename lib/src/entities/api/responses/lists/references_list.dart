import 'package:foodapp_api_splento/src/entities/api/responses/reference_item.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:json_annotation/json_annotation.dart';

part 'references_list.g.dart';

/// Photos List
///
@JsonSerializable(explicitToJson: true)
class ReferencesList {
  @JsonKey(name: 'num_of_results')
  int numOfResults;

  @JsonKey(name: 'num_of_pages')
  int numOfPages;

  int limit;

  int page;

  List<ReferenceItem> results;

  String error;

  ReferencesList({this.numOfResults, this.numOfPages, this.limit, this.page, List<ReferenceItem> results, this.error})
    : results = results ?? <ReferenceItem>[];

  factory ReferencesList.fromJson(Map<String, dynamic> json) => _$ReferencesListFromJson(json);

  Map<String, dynamic> toJson() => _$ReferencesListToJson(this);

}