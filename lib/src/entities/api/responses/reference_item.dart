import 'package:json_annotation/json_annotation.dart';

/// Reference Item
/// Guide to shooting guide images
///
part 'reference_item.g.dart';

@JsonSerializable(explicitToJson: true)
class ReferenceItem {

  String id;
  String name;
  @JsonKey(name: 'mode_id')
  String modeId;
  @JsonKey(name: 'company_id')
  String companyId;

  /// Number of dishes for each mode photo
  @JsonKey(name: 'num_of_dishes')
  int numOfDishes;
  /// Dishes Type
  /// [rounded], [square], [mixed]
  @JsonKey(name: 'dishes_shape')
  String dishesShape;

  /// Link to full image. Large (PNG 1035px by width)
  @JsonKey(name: 'reference_image_url')
  String referenceImageUrl;

  /// Link to full image. Medium (PNG 690px by width)
  @JsonKey(name: 'reference_image_medium_url')
  String referenceImageMediumUrl;

  /// Link to full image. Small (PNG 345px by width)
  @JsonKey(name: 'reference_image_small_url')
  String referenceImageSmallUrl;


  /// Link to full image. Large overlay (PNG 345px by width)
  @JsonKey(name: 'reference_overlay_url')
  String referenceOverlayUrl;

  /// Link to full image. Medium overlay (PNG 345px by width)
  @JsonKey(name: 'reference_overlay_medium_url')
  String referenceOverlayMediumUrl;

  /// Link to full image. Small overlay (PNG 345px by width)
  @JsonKey(name: 'reference_overlay_small_url')
  String referenceOverlaySmallUrl;




  String error;

  /// Relations
  ReferenceItem({ this.id, this.name, this.modeId, this.companyId,
    this.numOfDishes, this.dishesShape, this.referenceImageUrl,
    this.referenceImageMediumUrl, this.referenceImageSmallUrl,
    this.referenceOverlayUrl, this.referenceOverlayMediumUrl,
    this.referenceOverlaySmallUrl,
    this.error});

  factory ReferenceItem.fromJson(Map<String, dynamic> json) => _$ReferenceItemFromJson(json);

  Map<String, dynamic> toJson() => _$ReferenceItemToJson(this);


}