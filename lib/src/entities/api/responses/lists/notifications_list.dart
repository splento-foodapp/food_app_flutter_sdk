import 'package:foodapp_api_splento/src/entities/api/responses/notification_item.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:json_annotation/json_annotation.dart';

part 'notifications_list.g.dart';

/// Notification List
///
@JsonSerializable(explicitToJson: true)
class NotificationList {
  @JsonKey(name: 'num_of_results')
  int numOfResults;

  @JsonKey(name: 'num_of_pages')
  int numOfPages;

  int limit;

  int page;

  List<NotificationItem> results;

  String error;

  NotificationList({this.numOfResults, this.numOfPages, this.limit, this.page, List<NotificationItem> results, this.error})
    : results = results ?? <NotificationItem>[];

  factory NotificationList.fromJson(Map<String, dynamic> json) => _$NotificationListFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationListToJson(this);

}