// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserItem _$UserItemFromJson(Map<String, dynamic> json) {
  return UserItem(
    id: json['id'] as String,
    firstName: json['first_name'] as String,
    lastName: json['last_name'] as String,
    phone: json['phone'] as String,
    email: json['email'] as String,
    locationCoordinates: json['last_coord'] as String,
    locationName: json['location_name'] as String,
    groups: (json['groups'] as List)
        ?.map((e) => e == null
            ? null
            : UserGroupItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    companies: (json['companies'] as List)
        ?.map((e) =>
            e == null ? null : CompanyItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    menus: (json['menus'] as List)
        ?.map(
            (e) => e == null ? null : Menu.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$UserItemToJson(UserItem instance) => <String, dynamic>{
      'id': instance.id,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'email': instance.email,
      'phone': instance.phone,
      'last_coord': instance.locationCoordinates,
      'location_name': instance.locationName,
      'groups': instance.groups?.map((e) => e?.toJson())?.toList(),
      'companies': instance.companies?.map((e) => e?.toJson())?.toList(),
      'menus': instance.menus?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
