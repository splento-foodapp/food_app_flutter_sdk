import 'dart:io';

import 'package:flutter_test/flutter_test.dart';

import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/entities.dart';

import 'dart:developer' as developer;

void main() {
  group('[SPLENTO]: Rest service', ()
  {
    SPLSdk sdk;
    String menuId;
    ModeItem mode;
    PhotoItem photoForTest = PhotoItem();
    
    developer.log('Test Test');

    test('Rest Service - Init', () {
      sdk = SPLSdk();
      expect(sdk.rest.checkIfObjectIsNotNull(), true);
    });

    test('Check init is initialized', () async {
      await sdk.initializeData();
      expect(sdk.initData, isNot(null));
    });


    test('Rest Service - Init Action', () async {
      final initResponse =  await sdk.rest.initAction();
      sdk.initData = initResponse;
      expect(initResponse, isNot(null));
    });


    test('Rest Service - Success Login', () async {
      LoginRequest form = LoginRequest(email: testUser, password: testPassword, deviceId: '', devicePlatform: 'none');
      final lResponse = await sdk.rest.loginAction(form);
      expect(lResponse.token, isNot(null));
    });

    test('Rest Service - Success getting user Info', () async {
      final lResponse = await sdk.rest.userInfoAction(sdk.rest.getUserId());
      sdk.setUserData(lResponse);
      expect(lResponse.id, isNot(null));
    });

    /// PHOTO


    /// PHOTO CHECK
    test('Rest Service - Photo check', () async {
      var fileImage = new File('./assets/images/test_image.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'OK');
    });

    test('Rest Service - Photo check 2', () async {
      var fileImage = new File('./assets/images/food_image.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'OK');
    });

    test('Rest Service - Photo check 3', () async {
      var fileImage = new File('./assets/images/hamb_image.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'OK');
    });

    test('Rest Service - Photo check 4', () async {
      var fileImage = new File('./assets/images/blurred_image.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'FAIL');
    });

    /// EXTEND PHOTO CHECK
    test('Rest Service - Photo check EXT 1', () async {
      var fileImage = new File('./assets/images/ext/success_1.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'OK');
    });
    test('Rest Service - Photo check EXT 2', () async {
      var fileImage = new File('./assets/images/ext/underexposed.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'FAIL');
    });
    test('Rest Service - Photo check EXT 3', () async {
      var fileImage = new File('./assets/images/ext/over_exposed.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'FAIL');
    });
    test('Rest Service - Photo check EXT 4', () async {
      var fileImage = new File('./assets/images/ext/fail.jpg');
      PhotoCheckRequest phRequest = PhotoCheckRequest(photo: fileImage.readAsBytesSync());
      final lResponse = await sdk.rest.photoCheckAction(phRequest);
      expect(lResponse.status, 'FAIL');
    });
    /// EXTEND PHOTO CHECK END
    /// PHOTO CHECK END
    ///
    /// PHOTO ITEMS
    /// List photos
    test('Rest Service - List photos', () async {
      String menuId = sdk.getUserData().menus.first.id;
      final lResponse = await sdk.rest.photoListAction(menuId);
      expect(lResponse.error, null);
    });


    /// Create photo
    ///
    test('Rest Service - Create photo', () async {
      menuId = sdk.getUserData().menus.first.id;
      mode = sdk.getUserData().companies.first.modes.first;

      /// Fill photo data
      photoForTest.menuId = menuId;
      photoForTest.modeId = mode.id;
      photoForTest.fileName = 'from_unit_test.jpg';
      photoForTest.rawFileUrl = 'https://media.morozenko.com/New_Tab_2020-09-05_20-39-27.jpg';
      photoForTest.aspectRatioY = mode.aspectRatioY;
      photoForTest.aspectRatioX = mode.aspectRatioX;

      photoForTest.angleV = mode.angleV;
      photoForTest.angleH = mode.angleH;

      final lResponse = await sdk.rest.photoCreateAction(photoForTest);
       photoForTest = lResponse;
       expect(lResponse.id, isNot(null));
    });
    /// Get photo
    test('Rest Service - Get photo', () async {
      final lResponse = await sdk.rest.photoInfoAction(photoForTest.id);
      photoForTest = lResponse;
      expect(lResponse.error, null);
    });

    /// Get photo (fail)
    test('Rest Service - Get photo fail', () async {
      final lResponse = await sdk.rest.photoInfoAction('xxx');
      expect(lResponse.error, isNot(null));
    });

    /// Update photo
    test('Rest Service - Update photo', () async {
      String newFileName = 'from_unit_test_2.jpg';
      photoForTest.fileName = newFileName;
      final lResponse = await sdk.rest.photoUpdateAction(photoForTest);
      photoForTest = lResponse;
      expect(lResponse.fileName, newFileName);
    });
    /// Delete photo
    test('Rest Service - Delete photo', () async {
      final lResponse = await sdk.rest.photoDeleteAction(photoForTest);
      photoForTest = lResponse;
      expect(lResponse.error, null);
    });
    /// PHOTO ITEMS END

    /// PHOTO END







  });
}
