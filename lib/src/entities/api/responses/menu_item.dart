import 'package:json_annotation/json_annotation.dart';

import 'package:foodapp_api_splento/src/entities/api/responses/menu.dart';

part 'menu_item.g.dart';
/// Menu Item
/// Sub items for [Menu]
///

@JsonSerializable(explicitToJson: true)
class MenuItem {

  String id;
  String name;
  String description;
  @JsonKey(name: 'menu_id')
  String menuId;

  String error;


  MenuItem({ this.id, this.name, this.description, this.menuId, this.error});

  factory MenuItem.fromJson(Map<String, dynamic> json) => _$MenuItemFromJson(json);

  Map<String, dynamic> toJson() => _$MenuItemToJson(this);

}