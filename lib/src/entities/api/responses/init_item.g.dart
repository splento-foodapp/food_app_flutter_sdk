// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'init_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InitItem _$InitItemFromJson(Map<String, dynamic> json) {
  return InitItem(
    sortBy: json['sort_by'] as Map<String, dynamic>,
    limit: json['limit'] as Map<String, dynamic>,
    photoStatuses: json['photo_statuses'] as Map<String, dynamic>,
    automatedCheckStatuses:
        json['automated_check_statuses'] as Map<String, dynamic>,
    notificationTypes: json['notification_types'] as Map<String, dynamic>,
    dishShapeTypes: json['dish_shape_types'] as Map<String, dynamic>,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$InitItemToJson(InitItem instance) => <String, dynamic>{
      'sort_by': instance.sortBy,
      'limit': instance.limit,
      'photo_statuses': instance.photoStatuses,
      'automated_check_statuses': instance.automatedCheckStatuses,
      'notification_types': instance.notificationTypes,
      'dish_shape_types': instance.dishShapeTypes,
      'error': instance.error,
    };
