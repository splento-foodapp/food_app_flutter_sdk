import 'package:foodapp_api_splento/src/entities/api/responses/photo_item.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:json_annotation/json_annotation.dart';

part 'photos_list.g.dart';

/// Photos List
///
@JsonSerializable(explicitToJson: true)
class PhotosList {
  @JsonKey(name: 'num_of_results')
  int numOfResults;

  @JsonKey(name: 'num_of_pages')
  int numOfPages;

  int limit;

  int page;

  List<PhotoItem> results;

  String error;

  PhotosList({this.numOfResults, this.numOfPages, this.limit, this.page, List<PhotoItem> results, this.error})
    : results = results ?? <PhotoItem>[];

  factory PhotosList.fromJson(Map<String, dynamic> json) => _$PhotosListFromJson(json);

  Map<String, dynamic> toJson() => _$PhotosListToJson(this);

}