// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationItem _$NotificationItemFromJson(Map<String, dynamic> json) {
  return NotificationItem(
    id: json['id'] as String,
    companyId: json['company_id'] as String,
    userId: json['user_id'] as String,
    typeOfNotify: json['type_of_notify'] as String,
    name: json['name'] as String,
    sessionUrl: json['session_url'] as String,
    imageUrl: json['image_url'] as String,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$NotificationItemToJson(NotificationItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'company_id': instance.companyId,
      'user_id': instance.userId,
      'type_of_notify': instance.typeOfNotify,
      'session_url': instance.sessionUrl,
      'image_url': instance.imageUrl,
      'error': instance.error,
    };
