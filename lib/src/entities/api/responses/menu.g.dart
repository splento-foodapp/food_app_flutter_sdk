// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Menu _$MenuFromJson(Map<String, dynamic> json) {
  return Menu(
    id: json['id'] as String,
    name: json['name'] as String,
    companyId: json['company_id'] as String,
    ownerId: json['owner_id'] as String,
    photoItems: (json['photos'] as List)
        ?.map((e) =>
            e == null ? null : PhotoItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    menuItems: (json['menuitems'] as List)
        ?.map((e) =>
            e == null ? null : MenuItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    isDraft: json['is_draft'] as int,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$MenuToJson(Menu instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'company_id': instance.companyId,
      'owner_id': instance.ownerId,
      'is_draft': instance.isDraft,
      'photos': instance.photoItems?.map((e) => e?.toJson())?.toList(),
      'menuitems': instance.menuItems?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
