import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/entities.dart';
import 'package:foodapp_api_splento/src/handlers/handlers.dart';
import 'package:foodapp_api_splento/src/services/services.dart';

import 'package:uuid/uuid.dart';

import 'dart:developer' as developer;

void main() {
  group('[SPLENTO]: Rest service', ()
  {
    SPLSdk sdk;
    String menuId;
    ModeItem mode;
    ExampleItem exampleForTest = ExampleItem();
    
    developer.log('Test Test');

    test('Rest Service - Init', () {
      sdk = SPLSdk();
      expect(sdk.rest.checkIfObjectIsNotNull(), true);
    });

    test('Check init is initialized', () async {
      await sdk.initializeData();
      expect(sdk.initData, isNot(null));
    });


    test('Rest Service - Init Action', () async {
      final initResponse =  await sdk.rest.initAction();
      sdk.initData = initResponse;
      expect(initResponse, isNot(null));
    });


    test('Rest Service - Success Login', () async {
      LoginRequest form = LoginRequest(email: testUser, password: testPassword, deviceId: '', devicePlatform: 'none');
      final lResponse = await sdk.rest.loginAction(form);
      expect(lResponse.token, isNot(null));
    });

    test('Rest Service - Success getting user Info', () async {
      final lResponse = await sdk.rest.userInfoAction(sdk.rest.getUserId());
      sdk.setUserData(lResponse);
      expect(lResponse.id, isNot(null));
    });

  
    /// List
    test('Rest Service - List examples', () async {
      String modeId = sdk.getUserData().companies.first.modes.first.id;
      final lResponse = await sdk.rest.exampleListAction(modeId);
      if ( lResponse.results.isNotEmpty ) {
        exampleForTest = lResponse.results.first;
      }
      expect(lResponse.error, null);
    });


    /// Get example
    test('Rest Service - Get example', () async {
      if (exampleForTest.id != null) {
        final lResponse = await sdk.rest.exampleInfoAction(
            exampleForTest.id);
        exampleForTest = lResponse;
        expect(lResponse.error, null);
      } else {
        // Examples list is empty
        expect(true, true);
      }

    });







  });
}
