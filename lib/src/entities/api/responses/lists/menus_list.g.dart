// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menus_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MenusList _$MenusListFromJson(Map<String, dynamic> json) {
  return MenusList(
    numOfResults: json['num_of_results'] as int,
    numOfPages: json['num_of_pages'] as int,
    limit: json['limit'] as int,
    page: json['page'] as int,
    results: (json['results'] as List)
        ?.map(
            (e) => e == null ? null : Menu.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$MenusListToJson(MenusList instance) => <String, dynamic>{
      'num_of_results': instance.numOfResults,
      'num_of_pages': instance.numOfPages,
      'limit': instance.limit,
      'page': instance.page,
      'results': instance.results?.map((e) => e?.toJson())?.toList(),
      'error': instance.error,
    };
