import 'package:json_annotation/json_annotation.dart';

part 'example_item.g.dart';
/// Example Item
/// Sub Entity for [ModeItem] and [CompanyItem]
///
@JsonSerializable(explicitToJson: true)
class ExampleItem {

  String id;
  @JsonKey(name: 'company_id')
  String companyId;
  @JsonKey(name: 'mode_id')
  String modeId;

  /// Name for list
  String name;
  /// Full link for thumbnail
  @JsonKey(name: 'example_small_url')
  String exampleThumbUrl;
  /// Full link for (800px by width) image
  @JsonKey(name: 'example_url')
  String exampleUrl;




  String error;

  /// Relations
  ExampleItem({ this.id, this.companyId, this.modeId, this.name,
    this.exampleThumbUrl, this.exampleUrl, this.error});

  factory ExampleItem.fromJson(Map<String, dynamic> json) => _$ExampleItemFromJson(json);

  Map<String, dynamic> toJson() => _$ExampleItemToJson(this);

}