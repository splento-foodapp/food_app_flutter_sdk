import 'dart:developer';

import 'package:json_annotation/json_annotation.dart';

part 'photo_check_response.g.dart';

/// Photo Check Response
///
@JsonSerializable(explicitToJson: true)
class PhotoCheckResponse {

  /// Status operation [OK] or [FAIL]
  String status;

  /// List messages for view
  List<String> messages;

  PhotoCheckResponse({this.status, List<String> messages})
    : messages = messages ?? <String>[];


  factory PhotoCheckResponse.fromJson(Map<String, dynamic> json) => _$PhotoCheckResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoCheckResponseToJson(this);
}