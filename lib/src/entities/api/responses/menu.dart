import 'package:foodapp_api_splento/src/entities/api/responses/menu_item.dart';
import 'package:foodapp_api_splento/src/entities/api/responses/photo_item.dart';

import 'package:json_annotation/json_annotation.dart';

part 'menu.g.dart';

/// Menu
///
///
@JsonSerializable(explicitToJson: true)
class Menu {

  String id;
  String name;
  @JsonKey(name: 'company_id')
  String companyId;
  @JsonKey(name: 'owner_id')
  String ownerId;

  @JsonKey(name: 'is_draft')
  int isDraft;

  /// Relations
  /// Photos
  @JsonKey(name: 'photos')
  List<PhotoItem> photoItems;
  /// MenuItems
  @JsonKey(name: 'menuitems')
  List<MenuItem> menuItems;



  String error;

  /// Relations
  Menu({ this.id, this.name, this.companyId, this.ownerId,
    List<PhotoItem> photoItems, List<MenuItem> menuItems,
    this.isDraft, this.error})
      : photoItems = photoItems ?? <PhotoItem>[],
      menuItems = menuItems ?? <MenuItem>[];

  factory Menu.fromJson(Map<String, dynamic> json) => _$MenuFromJson(json);

  Map<String, dynamic> toJson() => _$MenuToJson(this);

}