import 'package:json_annotation/json_annotation.dart';

part 'user_group_item.g.dart';

/// User Group Item
@JsonSerializable(explicitToJson: true)
class UserGroupItem {

  String id;
  String name;

  String error;

  UserGroupItem({ this.id, this.name });

  factory UserGroupItem.fromJson(Map<String, dynamic> json) => _$UserGroupItemFromJson(json);

  Map<String, dynamic> toJson() => _$UserGroupItemToJson(this);


}