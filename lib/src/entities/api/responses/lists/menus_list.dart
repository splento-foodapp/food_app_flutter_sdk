import 'package:foodapp_api_splento/src/entities/api/responses/menu.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:json_annotation/json_annotation.dart';

part 'menus_list.g.dart';

/// Menus List
///
@JsonSerializable(explicitToJson: true)
class MenusList {
  @JsonKey(name: 'num_of_results')
  int numOfResults;

  @JsonKey(name: 'num_of_pages')
  int numOfPages;

  int limit;

  int page;

  List<Menu> results;

  String error;

  MenusList({this.numOfResults, this.numOfPages, this.limit, this.page, List<Menu> results, this.error})
    : results = results ?? <Menu>[];

  factory MenusList.fromJson(Map<String, dynamic> json) => _$MenusListFromJson(json);

  Map<String, dynamic> toJson() => _$MenusListToJson(this);

}