// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mode_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ModeItem _$ModeItemFromJson(Map<String, dynamic> json) {
  return ModeItem(
    id: json['id'] as String,
    name: json['name'] as String,
    companyId: json['company_id'] as String,
    isDishesRequired:
        ModeItem._convertToBool(json['is_dishes_required'] as int),
    isAutoAnalysisRequired:
        ModeItem._convertToBool(json['is_auto_analys_required'] as int),
    isExamplesRequired:
        ModeItem._convertToBool(json['is_examples_required'] as int),
    isFoodRecognitionRequired:
        ModeItem._convertToBool(json['is_food_recognition_required'] as int),
    guidelineUrl: json['guideline_url'] as String,
    aspectRatioX: json['aspect_ratio_x'] as int,
    aspectRatioY: json['aspect_ratio_y'] as int,
    angleV: json['angle_v'] as int,
    angleH: json['angle_h'] as int,
    maxAngleVDeviation: json['max_angle_v_deviation'] as int,
    maxAngleHDeviation: json['max_angle_h_deviation'] as int,
  )..error = json['error'] as String;
}

Map<String, dynamic> _$ModeItemToJson(ModeItem instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'company_id': instance.companyId,
      'is_dishes_required':
          ModeItem._convertFromBool(instance.isDishesRequired),
      'is_auto_analys_required':
          ModeItem._convertFromBool(instance.isAutoAnalysisRequired),
      'is_examples_required':
          ModeItem._convertFromBool(instance.isExamplesRequired),
      'is_food_recognition_required':
          ModeItem._convertFromBool(instance.isFoodRecognitionRequired),
      'guideline_url': instance.guidelineUrl,
      'aspect_ratio_x': instance.aspectRatioX,
      'aspect_ratio_y': instance.aspectRatioY,
      'angle_v': instance.angleV,
      'angle_h': instance.angleH,
      'max_angle_v_deviation': instance.maxAngleVDeviation,
      'max_angle_h_deviation': instance.maxAngleHDeviation,
      'error': instance.error,
    };
