import 'package:json_annotation/json_annotation.dart';

part 'notification_item.g.dart';
/// Notification Item
///

@JsonSerializable(explicitToJson: true)
class NotificationItem {

  String id;
  String name;
  @JsonKey(name: 'company_id')
  String companyId;
  @JsonKey(name: 'user_id')
  String userId;

  /// One of type [accepted], [rejected], [submitted]
  @JsonKey(name: 'type_of_notify')
  String typeOfNotify;
  /// Session full URL for show results to user
  @JsonKey(name: 'session_url')
  String sessionUrl;
  /// Full image preview URL
  @JsonKey(name: 'image_url')
  String imageUrl;

  String error;


  NotificationItem({ this.id, this.companyId, this.userId, this.typeOfNotify,
    this.name, this.sessionUrl, this.imageUrl,
    this.error});

  factory NotificationItem.fromJson(Map<String, dynamic> json) => _$NotificationItemFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationItemToJson(this);


}