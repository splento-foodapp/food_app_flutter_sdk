import 'dart:developer';
import 'dart:io';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/entities.dart';
import 'package:foodapp_api_splento/src/handlers/handlers.dart';
import 'package:foodapp_api_splento/src/services/services.dart';



import 'package:uuid/uuid.dart';

import 'dart:developer' as developer;

void main() {

  group('[SPLENTO]: Rest service', ()
  {
    SPLSdk sdk;

    test('Rest Service - Init', () {
      sdk = SPLSdk();
      expect(sdk.rest.checkIfObjectIsNotNull(), true);
    });

    test('Check init is initialized', () async {
      await sdk.initializeData();
      expect(sdk.initData, isNot(null));
    });


    test('Rest Service - Init Action', () async {
      final initResponse =  await sdk.rest.initAction();
      sdk.initData = initResponse;
      expect(initResponse, isNot(null));
    });

    test('Rest Service - Login Failed', () async {
      LoginRequest form = LoginRequest(email: '', password: '', deviceId: '', devicePlatform: 'none');
      final lResponse = await sdk.rest.loginAction(form);
      expect(lResponse.error, isNot(null));
    });

    test('Rest Service - Success Login', () async {
      LoginRequest form = LoginRequest(email: testUser, password: testPassword, deviceId: '', devicePlatform: 'none');
      final lResponse = await sdk.rest.loginAction(form);
      expect(lResponse.token, isNot(null));
    });

    test('Rest Service - Failure getting user Info', () async {
      final lResponse = await sdk.rest.userInfoAction("");
      expect(lResponse.error, isNot(null));
    });

    test('Rest Service - Success getting user Info', () async {
      final lResponse = await sdk.rest.userInfoAction(sdk.rest.getUserId());
      expect(lResponse.id, isNot(null));
    });



  });
}
