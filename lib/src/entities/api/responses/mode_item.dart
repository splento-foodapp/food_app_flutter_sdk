import 'package:json_annotation/json_annotation.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';

part 'mode_item.g.dart';
/// Mode Item
///  Shooting photos mode item
///

@JsonSerializable(explicitToJson: true)
class ModeItem {

  String id;
  String name;
  @JsonKey(name: 'company_id')
  final String companyId;

  /// Application should make request for the number and shape dishes
  @JsonKey(name: 'is_dishes_required', fromJson: _convertToBool, toJson: _convertFromBool)
  final bool isDishesRequired;
  /// Application should call [SPLRest.PhotoCheck] method before save image
  @JsonKey(name: 'is_auto_analys_required', fromJson: _convertToBool, toJson: _convertFromBool)
  final bool isAutoAnalysisRequired;
  /// Applications should show examples for user
  @JsonKey(name: 'is_examples_required', fromJson: _convertToBool, toJson: _convertFromBool)
  final bool isExamplesRequired;
  /// Applications should show food recognition button
  @JsonKey(name: 'is_food_recognition_required', fromJson: _convertToBool, toJson: _convertFromBool)
  final bool isFoodRecognitionRequired;

  /// Applications should show Guideline for user by url
  @JsonKey(name: 'guideline_url')
  final String guidelineUrl;

  /// Aspect ratio width
  @JsonKey(name: 'aspect_ratio_x')
  final int aspectRatioX;
  /// Aspect ration height
  @JsonKey(name: 'aspect_ratio_y')
  final int aspectRatioY;

  /// Required vertical shooting angle ( deg. ) range -90 <-> +90 deg.
  @JsonKey(name: 'angle_v')
  final int angleV;
  /// Required horizontal shooting angle ( deg. ) range -90 <-> +90  deg.
  @JsonKey(name: 'angle_h')
  final int angleH;


  /// Max. deviation for vertical shooting angle ( ex. angleV = 10,
  /// maxAngleVDeviation = 20 - Deviation - from +30 deg. to -30 deg. )
  @JsonKey(name: 'max_angle_v_deviation')
  final int maxAngleVDeviation;
  /// Max. deviation for horizontal shooting angle ( ex. angleH = 45,
  /// maxAngleVDeviation = 10 - Deviation - from +55 deg. to +35 deg. )
  @JsonKey(name: 'max_angle_h_deviation')
  final int maxAngleHDeviation;


  String error;


  ModeItem({ this.id, this.name, this.companyId, this.isDishesRequired,
    this.isAutoAnalysisRequired, this.isExamplesRequired,
    this.isFoodRecognitionRequired, this.guidelineUrl,
    this.aspectRatioX, this.aspectRatioY, this.angleV, this.angleH,
    this.maxAngleVDeviation, this.maxAngleHDeviation});

   static bool _convertToBool(int val) {
    return val == 1 ? true : false;
  }

  static int _convertFromBool(bool val) {
    return val ? 1 : 0;
  }

  factory ModeItem.fromJson(Map<String, dynamic> json) => _$ModeItemFromJson(json);
  Map<String, dynamic> toJson() => _$ModeItemToJson(this);


}