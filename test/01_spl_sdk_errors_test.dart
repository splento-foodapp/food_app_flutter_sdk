import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/entities.dart';
import 'package:foodapp_api_splento/src/handlers/handlers.dart';

import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:uuid/uuid.dart';

void main() {
  group('[SPLENTO]: Error service', ()
  {
    SPLSdk sdk;

    test('Errors Service - Init', () {
      sdk = SPLSdk();
      expect(sdk.errors.checkIfObjectIsNotNull(), true);
    });

    test('Errors Service - toString', () {
      expect(sdk.errors.toString(), isNot(""));
    });

    test('Errors Service - init errors', () {
      expect(sdk.errors.listErrors, isNot(null));
    });


    test('Errors Service - last error is null', () {
      expect(sdk.errors.lastError, null);
    });

    // Check functionality

    var uuid = Uuid();
    var message = "Simple Error message";

    var uuidStr = uuid.v4();

    test('Errors Service - storeError', () {
      var error = SPLError(uuidStr, message);
      sdk.errors.storeError(uuidStr, error);
      expect(sdk.errors.listErrors.length, 1);
      expect(sdk.errors.lastError, isNot(null));
    });

    test('Errors Service - getError', () {
      expect(sdk.errors.lastError, isNot(null));
      expect(sdk.errors.getError(uuidStr).message, "Simple Error message");
    });

    test('Errors Service - getLastError', () {
      expect(sdk.errors.lastError, isNot(null));
      expect(sdk.errors.getLastError().message, "Simple Error message");
    });

    test('Errors Service - toString', () {
      expect(sdk.errors.toString(), contains("[SPLENTO]: Errors Service - contains 1"));
    });

    test('Errors Service - removeError', () {
      expect(sdk.errors.removeError(uuidStr), null);

    });

    test('Errors Service - get after remove', () {
      expect(() => sdk.errors.getError(uuidStr), throwsException);
    });


    test('Errors Service - getLastError', () {
      expect(sdk.errors.lastError, null);
      expect(sdk.errors.getLastError(), null);
    });

  });
}
