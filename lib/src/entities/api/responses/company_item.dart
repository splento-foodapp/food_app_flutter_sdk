import 'mode_item.dart';
import 'reference_item.dart';
import 'example_item.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:foodapp_api_splento/spl_sdk.dart';
import 'package:foodapp_api_splento/src/entities/api/responses/example_item.dart';

part 'company_item.g.dart';
/// Company Item
///
///

@JsonSerializable(explicitToJson: true)
class CompanyItem {

  String id;
  String name;
  String email;
  @JsonKey(name: 'owner_id')
  String ownerId;
  @JsonKey(name: 'invite_code')
  String inviteCode;


  /// Relations
  List<ModeItem> modes;
  List<ReferenceItem> references;
  List<ExampleItem> examples;


  /// Extend
  String error;


  CompanyItem({ this.id, this.name, this.email, this.ownerId,
    this.inviteCode, List<ModeItem> modes, List<ReferenceItem> references,
    List<ExampleItem> examples,
    this.error
  }):
    modes = modes ?? <ModeItem>[],
    references = references ?? <ReferenceItem>[],
    examples = examples ?? <ExampleItem>[];

  factory CompanyItem.fromJson(Map<String, dynamic> json) => _$CompanyItemFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyItemToJson(this);

}