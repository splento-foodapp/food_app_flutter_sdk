// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_check_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoCheckResponse _$PhotoCheckResponseFromJson(Map<String, dynamic> json) {
  return PhotoCheckResponse(
    status: json['status'] as String,
    messages: (json['messages'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$PhotoCheckResponseToJson(PhotoCheckResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'messages': instance.messages,
    };
