## [0.0.1] - 05/09/2020

- Initialization data ( Info )
- Login / Logout User
- User Info ( Info )
- Company data ( in UserInfo )
- Menus ( List, Info )
- Menu Items (CRUD)
- Notifications ( List, Info )
- References ( List, Info )
- Examples ( List, Info )
- Photos (CRUD)

## [0.0.1] - 08/09/2020

- Updated Photo check unit tests

## [0.0.1]  - 10/09/2020

- References model was updated ( added: overlay, extend links )
- Tests were updated
