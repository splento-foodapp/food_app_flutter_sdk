library foodapp_api_splento;

import 'src/src.dart';
export 'src/src.dart';

import 'dart:developer';

/// SPLENTO Food Api SDK ( package )
///
/// This library included, objects and methods for work with SPLENTO API (REST)
/// before you start call any method you must call initializeData at least once
///
///
class SPLSdk {
  /// Instance for [SPLRest] Service ( work with API points )
  SPLRest rest = SPLRest();
  /// Instance for [SPLErrors] Service ( work with API points )
  SPLErrors errors = SPLErrors();

  static final SPLSdk _instance = SPLSdk._internal();

  InitItem initData;

  String _userId;
  String _token;
  UserItem _user;

  factory SPLSdk() {
    _instance.initializeData();
    return _instance;
  }

  SPLSdk._internal();

  /// For change debug|live mode
  void debugMode(bool flag) {
     isDebug = flag ?? false;
  }

  /// Get [SPLError] object from [SPLErrors] service
  SPLError getError(String uuid) {
    return this.errors.getError(uuid);
  }

  /// Get last error returned from API, or null
  SPLError getLastErrorFromApi() {
    if ( this.rest.lastErrorUuid.isNotEmpty ) {
      return this.errors.getError(this.rest.lastErrorUuid);
    }
    return null;
  }

  /// Initialization Method - must be called
  void initializeData() async {
    final iResponse = await this.rest.initAction();
    if ( iResponse.error != null && iResponse.error != "" ) {
      throw Exception(iResponse.error);
    }
    this.initData = iResponse;
  }

  /// Login with email and password  data, and deviceID
  /// ( Token for Push notification ),
  /// and devicePlatform ( String - ios||android )
  /// and fill user info if login is success
  /// return true if init data stored, login is success
  /// and the user data have been obtained
  /// It will return [Exception] if email or password is empty
  Future<bool> login(String email, String password, String deviceId, String devicePlatform) async {
    bool success = false;
    if ( email.isEmpty || password.isEmpty ) {
      throw Exception("Email and password are required field");
    }

    /// Init data
    final iResponse = await this.rest.initAction();
    if ( iResponse.error != null && iResponse.error.isNotEmpty ) {
      throw Exception(iResponse.error);
    }
    this.initData = iResponse;

    /// Call Login
    LoginRequest form = LoginRequest(email: email, password: password, deviceId: deviceId, devicePlatform: devicePlatform);
    final lResponse =  await this.rest.loginAction(form);
    if ( iResponse.error != null && lResponse.error.isNotEmpty ) {
      return success;
    }


    if ( lResponse.userId == null || lResponse.token == null ) {
      return false;
    }
    this.setToken(lResponse.token);
    this.setUserId(lResponse.userId);

    /// Call User info Method and fill user data
    final uResponse = await this.rest.userInfoAction(this.getUserId());
    if ( uResponse.error != null && uResponse.error.isNotEmpty ) {
      return success;
    }
    this.setUserData(uResponse);
    success = true;


    return success;
  }

  /// Set current user id
  void setUserId(String id) {
    if ( id == null || id == "" ) {
      this._user = null;
      this._token = null;
    }
    this._userId  = id;
  }
  /// Get current user id
  String getUserId() {
    return this._userId;
  }
  /// Set token for current user
  void setToken(String token) {
    if ( token == null || token == "" ) {
      this._user = null;
      this._userId = null;
    }

    this._token = token;
  }

  /// Get token for current user
  String getToken() {
    return this._token;
  }

  /// Set [UserItem] for current user
  void setUserData(UserItem user) {
    this._user = user;
  }

  /// Get [UserItem] for current user
  UserItem getUserData() {
    if ( this._userId == null && this._userId == "" ) {
      throw Exception("Before you can get this data "
          "you should call [rest.loginAction]");
    }
    return this._user;
  }

  void _checkInitData() {
    if ( this.initData == null ) {
      throw Exception("Initialization Data is empty, "
          "please call initializeData()");
    }
  }

}





